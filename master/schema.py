import graphene
import django_filters

from django.db import IntegrityError
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from graphene_file_upload.scalars import Upload
from graphql_relay import from_global_id
from graphql import GraphQLError

from .models import ItemStatus

from graphene import Connection, Int, Node

class ExtendedConnection(Connection):
    
    class Meta:
        abstract = True

    total_count = Int()
    edge_count = Int()

    def resolve_total_count(root, info, **kwargs):
        return root.length

    def resolve_edge_count(root, info, **kwargs):
        return len(root.edges)

# schema for ItemStatus model
# -------------------------------------------------------
class ItemStatusType(DjangoObjectType):
    """
    Type for the related model defined in the Meta class.
    """
    class Meta:
        model = ItemStatus
    
    
class ItemStatusFilter(django_filters.FilterSet):
    """
    Relay allows filtering of data by using filters.
    The fields defined are the filter sets which can be used as arguments when querying the data.
    """
    icontains_name = django_filters.CharFilter(lookup_expr='icontains',field_name='name')
    exact_name = django_filters.CharFilter(lookup_expr='exact',field_name='name')
    icontains_code = django_filters.CharFilter(lookup_expr='icontains',field_name='code')
    exact_code = django_filters.CharFilter(lookup_expr='exact',field_name='code')

    class Meta:
        model = ItemStatus
        fields = "__all__"


class ItemStatusNode(DjangoObjectType):
    """
    The Node class exposes data in a Relay framework.
    The data structure is a bit different from the normal GraphQL data structure.
    The data is exposed through Edges and Nodes.
    
    Edges: Represents a collection of nodes, which has pagination properties.
    
    Node: Are the final object or and edge for a new list of objects.
    Graphene will automatically map the Document model's fields onto the DocumentNode.
    This is configured in the DocumentNode's Meta class.
    """

    class Meta:
        model = ItemStatus
        interfaces = (graphene.relay.Node,)
        connection_class = ExtendedConnection
        
        
class ItemStatusConnectionField(DjangoFilterConnectionField):
    """
    Subclass of DjangoFilterConnectionField.
    Defines the connection field, which implements the pagination structure.
    """

    class Meta:
        model = ItemStatus
        filter_class = ItemStatusFilter
        
    
class CreateItemStatus(graphene.relay.ClientIDMutation):
    """
    Mutation for creating a new Item Status.
    """
    item_status = graphene.Field(ItemStatusNode)
    
    class Input:
        code = graphene.String(required=True, description="Item Status code. Max 3 characters")
        name = graphene.String(required=True, description="Item Status name. Max 254 characters")
        
    def mutate_and_get_payload(root, info, **input):
        item_status = ItemStatus(**input)
        user = info.context.user
        item_status.created_by = user
        item_status.updated_by = user
        item_status.save()
        return CreateItemStatus(item_status=item_status)
    
    
class UpdateItemStatus(graphene.relay.ClientIDMutation):
    """
    Mutation for updating an Item Status.
    """
    item_status = graphene.Field(ItemStatusNode)
    
    class Input:
        id = graphene.ID(required=True, description="Item Status ID.")
        code = graphene.String(description="Item Status code. Max 3 characters")
        name = graphene.String(description="Item Status name. Max 254 characters")
        
    def mutate_and_get_payload(root, info, **input):
        id = input.get("id")
        try:
            item_status = ItemStatus.objects.get(pk=from_global_id(id)[1])
            if input.get("code"):
                item_status.code = input.get("code")
            if input.get("name"):
                item_status.name = input.get("name")
            item_status.save()
        except:
            raise Exception("The Item Status does not exist.")
        
        return UpdateItemStatus(item_status=item_status)
    
    
class DeleteItemStatus(graphene.relay.ClientIDMutation):
    """
    Mutation for deleting an Item Status.
    """
    success = graphene.Boolean()
    
    class Input:
        id = graphene.ID(required=True, description="Item Status ID")
    
    def mutate_and_get_payload(root, info, **input):
        try:
            item_status = ItemStatus.objects.get(pk=from_global_id(input.get("id"))[1])
            item_status.delete()
        except:
            Exception("The Item Status you are trying to delete does not exist")
        
        return DeleteItemStatus(success=True)
            
# -------------------------------------------------------

class Query(graphene.ObjectType):
    item_status = graphene.relay.Node.Field(ItemStatusNode, description="Returns an ItemStatus by the given id or an error if ItemStatus does not exist.")
    all_item_statuses = DjangoFilterConnectionField(ItemStatusNode, filterset_class=ItemStatusFilter, description="Returns a list of ItemStatus Types")

class Mutation(graphene.ObjectType):
    create_item_status = CreateItemStatus.Field()
    update_item_status = UpdateItemStatus.Field()
    delete_item_status = DeleteItemStatus.Field()