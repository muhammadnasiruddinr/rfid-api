from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

# Create your models here.

class ItemStatus(models.Model):

    code = models.CharField(_("item status code"), max_length=3, unique=True)
    name =  models.CharField(_("item status name"), max_length=254, unique=True)
    created_at = models.DateTimeField(
        _("created at"), auto_now_add=True, editable=False
    )
    updated_at = models.DateTimeField(_("created at"), auto_now=True, editable=False)
    created_by = models.CharField(
        _("created by"), max_length=254, default="system", editable=False
    )
    updated_by = models.CharField(
        _("updated by"), max_length=254, default="system", editable=False
    )

    class Meta:
        verbose_name = _("item status")
        verbose_name_plural = _("items status")
