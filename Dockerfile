# FROM python:3.9-alpine
FROM python:3.9
ENV PYTHONUNBUFFERED 1

# Install native libraries, required for numpy
# RUN apk --no-cache add musl-dev linux-headers g++

# Upgrade pip
RUN pip install --upgrade pip 

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . . 

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]