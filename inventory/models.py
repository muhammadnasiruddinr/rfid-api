from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from master.models import ItemStatus

# Create your models here.

class InventoryItem(models.Model):

    code = models.CharField(_("inventory item code"), max_length=3, unique=True)
    name =  models.CharField(_("inventory item name"), max_length=254, unique=True)
    description = models.TextField(_("description of the inventory item"))
    created_at = models.DateTimeField(
        _("created at"), auto_now_add=True, editable=False
    )
    updated_at = models.DateTimeField(_("created at"), auto_now=True, editable=False)
    created_by = models.CharField(
        _("created by"), max_length=254, default="system", editable=False
    )
    updated_by = models.CharField(
        _("updated by"), max_length=254, default="system", editable=False
    )

    class Meta:
        verbose_name = _("inventory item")
        verbose_name_plural = _("inventory items")


class InventoryLocation(models.Model):

    name =  models.CharField(_("inventory location name"), max_length=254, unique=True)
    description = models.TextField(_("description of the inventory location"))
    created_at = models.DateTimeField(
        _("created at"), auto_now_add=True, editable=False
    )
    updated_at = models.DateTimeField(_("created at"), auto_now=True, editable=False)
    created_by = models.CharField(
        _("created by"), max_length=254, default="system", editable=False
    )
    updated_by = models.CharField(
        _("updated by"), max_length=254, default="system", editable=False
    )

    class Meta:
        verbose_name = _("inventory location")
        verbose_name_plural = _("inventory location")

class Inventory(models.Model):
    
    item = models.ForeignKey(InventoryItem, verbose_name=_("inventory item"), on_delete=models.CASCADE)
    tag =  models.CharField(_("RFID Tag"), max_length=254, unique=True)
    serial_no = models.IntegerField(_("serial no"),blank=True, null=True)
    seat_no = models.CharField(_("RFID Tag"), max_length=254, blank=True, null=True)
    location = models.ForeignKey(InventoryLocation, verbose_name=_("inventory location"), on_delete=models.CASCADE)
    status = models.ForeignKey(ItemStatus,verbose_name=_("item status"), on_delete=models.CASCADE)
    created_at = models.DateTimeField(
        _("created at"), auto_now_add=True, editable=False
    )
    updated_at = models.DateTimeField(_("created at"), auto_now=True, editable=False)
    created_by = models.CharField(
        _("created by"), max_length=254, default="system", editable=False
    )
    updated_by = models.CharField(
        _("updated by"), max_length=254, default="system", editable=False)

    
    class Meta:
        verbose_name = _("inventory item")
        verbose_name_plural = _("inventory items")


