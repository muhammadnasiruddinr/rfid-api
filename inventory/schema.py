import graphene
import django_filters

from django.db import IntegrityError
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from graphene_file_upload.scalars import Upload
from graphql_relay import from_global_id
from graphql import GraphQLError

from .models import InventoryItem, InventoryLocation, Inventory
from master.models import ItemStatus

from graphene import Connection, Int, Node

class ExtendedConnection(Connection):
    
    class Meta:
        abstract = True

    total_count = Int()
    edge_count = Int()

    def resolve_total_count(root, info, **kwargs):
        return root.length

    def resolve_edge_count(root, info, **kwargs):
        return len(root.edges)

# schema for InventoryItem model
# -------------------------------------------------------
class InventoryItemType(DjangoObjectType):
    """
    Type for the related model defined in the Meta class.
    """
    class Meta:
        model = InventoryItem
    
    
class InventoryItemFilter(django_filters.FilterSet):
    """
    Relay allows filtering of data by using filters.
    The fields defined are the filter sets which can be used as arguments when querying the data.
    """
    icontains_name = django_filters.CharFilter(lookup_expr='icontains',field_name='name')
    exact_name = django_filters.CharFilter(lookup_expr='exact',field_name='name')
    icontains_code = django_filters.CharFilter(lookup_expr='icontains',field_name='code')
    exact_code = django_filters.CharFilter(lookup_expr='exact',field_name='code')

    class Meta:
        model = InventoryItem
        fields = "__all__"


class InventoryItemNode(DjangoObjectType):
    """
    The Node class exposes data in a Relay framework.
    The data structure is a bit different from the normal GraphQL data structure.
    The data is exposed through Edges and Nodes.
    
    Edges: Represents a collection of nodes, which has pagination properties.
    
    Node: Are the final object or and edge for a new list of objects.
    Graphene will automatically map the Document model's fields onto the DocumentNode.
    This is configured in the DocumentNode's Meta class.
    """

    class Meta:
        model = InventoryItem
        interfaces = (graphene.relay.Node,)
        connection_class = ExtendedConnection
        
        
class InventoryItemConnectionField(DjangoFilterConnectionField):
    """
    Subclass of DjangoFilterConnectionField.
    Defines the connection field, which implements the pagination structure.
    """

    class Meta:
        model = InventoryItem
        filter_class = InventoryItemFilter
        
    
class CreateInventoryItem(graphene.relay.ClientIDMutation):
    """
    Mutation for creating a new Inventory Item.
    """
    inventory_item = graphene.Field(InventoryItemNode)
    
    class Input:
        code = graphene.String(required=True, description="Inventory Item code. Max 3 characters")
        name = graphene.String(required=True, description="Inventory Item name. Max 254 characters")
        
    def mutate_and_get_payload(root, info, **input):
        inventory_item = InventoryItem(**input)
        user = info.context.user
        inventory_item.created_by = user
        inventory_item.updated_by = user
        inventory_item.save()
        return CreateInventoryItem(inventory_item=inventory_item)
    
    
class UpdateInventoryItem(graphene.relay.ClientIDMutation):
    """
    Mutation for updating an Inventory Item.
    """
    inventory_item = graphene.Field(InventoryItemNode)
    
    class Input:
        id = graphene.ID(required=True, description="Inventory Item ID.")
        code = graphene.String(description="Inventory Item code. Max 3 characters")
        name = graphene.String(description="Inventory Item name. Max 254 characters")
        
    def mutate_and_get_payload(root, info, **input):
        id = input.get("id")
        try:
            inventory_item = InventoryItem.objects.get(pk=from_global_id(id)[1])
            if input.get("code"):
                inventory_item.code = input.get("code")
            if input.get("name"):
                inventory_item.name = input.get("name")
            inventory_item.save()
        except:
            raise Exception("The Inventory Item does not exist.")
        
        return UpdateInventoryItem(inventory_item=inventory_item)
    
    
class DeleteInventoryItem(graphene.relay.ClientIDMutation):
    """
    Mutation for deleting an Inventory Item.
    """
    success = graphene.Boolean()
    
    class Input:
        id = graphene.ID(required=True, description="Inventory Item ID")
    
    def mutate_and_get_payload(root, info, **input):
        try:
            inventory_item = InventoryItem.objects.get(pk=from_global_id(input.get("id"))[1])
            inventory_item.delete()
        except:
            Exception("The Inventory Item you are trying to delete does not exist")
        
        return DeleteInventoryItem(success=True)
            
# -------------------------------------------------------
# schema for InventoryLocation model
# -------------------------------------------------------
class InventoryLocationType(DjangoObjectType):
    """
    Type for the related model defined in the Meta class.
    """
    class Meta:
        model = InventoryLocation
    
    
class InventoryLocationFilter(django_filters.FilterSet):
    """
    Relay allows filtering of data by using filters.
    The fields defined are the filter sets which can be used as arguments when querying the data.
    """
    icontains_name = django_filters.CharFilter(lookup_expr='icontains',field_name='name')
    exact_name = django_filters.CharFilter(lookup_expr='exact',field_name='name')
    icontains_description = django_filters.CharFilter(lookup_expr='icontains',field_name='description')
    exact_description = django_filters.CharFilter(lookup_expr='exact',field_name='description')

    class Meta:
        model = InventoryLocation
        fields = "__all__"


class InventoryLocationNode(DjangoObjectType):
    """
    The Node class exposes data in a Relay framework.
    The data structure is a bit different from the normal GraphQL data structure.
    The data is exposed through Edges and Nodes.
    
    Edges: Represents a collection of nodes, which has pagination properties.
    
    Node: Are the final object or and edge for a new list of objects.
    Graphene will automatically map the Document model's fields onto the DocumentNode.
    This is configured in the DocumentNode's Meta class.
    """

    class Meta:
        model = InventoryLocation
        interfaces = (graphene.relay.Node,)
        connection_class = ExtendedConnection
        
        
class InventoryLocationConnectionField(DjangoFilterConnectionField):
    """
    Subclass of DjangoFilterConnectionField.
    Defines the connection field, which implements the pagination structure.
    """

    class Meta:
        model = InventoryLocation
        filter_class = InventoryLocationFilter
        
    
class CreateInventoryLocation(graphene.relay.ClientIDMutation):
    """
    Mutation for creating a new Inventory Location.
    """
    inventory_location = graphene.Field(InventoryLocationNode)
    
    class Input:
        name = graphene.String(required=True, description="Inventory Location name. Max 3 characters")
        description = graphene.String(required=True, description="Inventory Location description. Max 254 characters")
        
    def mutate_and_get_payload(root, info, **input):
        inventory_location = InventoryLocation(**input)
        user = info.context.user
        inventory_location.created_by = user
        inventory_location.updated_by = user
        inventory_location.save()
        return CreateInventoryLocation(inventory_location=inventory_location)
    
    
class UpdateInventoryLocation(graphene.relay.ClientIDMutation):
    """
    Mutation for updating an Inventory Location.
    """
    inventory_location = graphene.Field(InventoryLocationNode)
    
    class Input:
        id = graphene.ID(required=True, description="Inventory Location ID.")
        name = graphene.String(description="Inventory Location name. Max 3 characters")
        description = graphene.String(description="Inventory Location description. Max 254 characters")
        
    def mutate_and_get_payload(root, info, **input):
        id = input.get("id")
        try:
            inventory_location = InventoryLocation.objects.get(pk=from_global_id(id)[1])
            if input.get("name"):
                inventory_location.name = input.get("name")
            if input.get("description"):
                inventory_location.description = input.get("description")
            inventory_location.save()
        except:
            raise Exception("The Inventory Location does not exist.")
        
        return UpdateInventoryLocation(inventory_location=inventory_location)
    
    
class DeleteInventoryLocation(graphene.relay.ClientIDMutation):
    """
    Mutation for deleting an Inventory Location.
    """
    success = graphene.Boolean()
    
    class Input:
        id = graphene.ID(required=True, description="Inventory Location ID")
    
    def mutate_and_get_payload(root, info, **input):
        try:
            inventory_location = InventoryLocation.objects.get(pk=from_global_id(input.get("id"))[1])
            inventory_location.delete()
        except:
            Exception("The Inventory Location you are trying to delete does not exist")
        
        return DeleteInventoryLocation(success=True)
            
# -------------------------------------------------------
# schema for Inventory model
# -------------------------------------------------------
class InventoryType(DjangoObjectType):
    """
    Type for the related model defined in the Meta class.
    """
    class Meta:
        model = Inventory
    
    
class InventoryFilter(django_filters.FilterSet):
    """
    Relay allows filtering of data by using filters.
    The fields defined are the filter sets which can be used as arguments when querying the data.
    """
    icontains_name = django_filters.CharFilter(lookup_expr='icontains',field_name='name')
    exact_name = django_filters.CharFilter(lookup_expr='exact',field_name='name')
    icontains_description = django_filters.CharFilter(lookup_expr='icontains',field_name='description')
    exact_description = django_filters.CharFilter(lookup_expr='exact',field_name='description')

    class Meta:
        model = Inventory
        fields = "__all__"


class InventoryNode(DjangoObjectType):
    """
    The Node class exposes data in a Relay framework.
    The data structure is a bit different from the normal GraphQL data structure.
    The data is exposed through Edges and Nodes.
    
    Edges: Represents a collection of nodes, which has pagination properties.
    
    Node: Are the final object or and edge for a new list of objects.
    Graphene will automatically map the Document model's fields onto the DocumentNode.
    This is configured in the DocumentNode's Meta class.
    """

    class Meta:
        model = Inventory
        interfaces = (graphene.relay.Node,)
        connection_class = ExtendedConnection
        
        
class InventoryConnectionField(DjangoFilterConnectionField):
    """
    Subclass of DjangoFilterConnectionField.
    Defines the connection field, which implements the pagination structure.
    """

    class Meta:
        model = Inventory
        filter_class = InventoryFilter
        
    
class CreateInventory(graphene.relay.ClientIDMutation):
    """
    Mutation for creating a new Inventory Location.
    """
    inventory= graphene.Field(InventoryNode)
    
    class Input:
        item = graphene.ID(required=True, description="Inventory item ID" )
        tag = graphene.String(required=True, description="RFID Tag")
        serial_no = graphene.String(description="Inventory Serial No. Max 254 characters")
        seat_no = graphene.String(description="Inventory seat no location")
        location = graphene.ID(required=True, description="Inventory location ID" )
        status = graphene.ID(required=True, description="Inventory status" )
        
    def mutate_and_get_payload(root, info, **input):

        item = InventoryItem.objects.get(pk=from_global_id(input.get("item"))[1])
        location = InventoryLocation.objects.get(pk=from_global_id(input.get("location"))[1])
        status = ItemStatus.objects.get(pk=from_global_id(input.get("status"))[1])

        inventory= Inventory(
            item = item,
            tag = input.get("tag"),
            serial_no = input.get("serial_no"),
            seat_no = input.get("seat_no"),
            location = location,
            status = status
        )
        user = info.context.user
        inventory.created_by = user
        inventory.updated_by = user
        inventory.save()
        return CreateInventory(inventory=inventory)
    
    
class UpdateInventory(graphene.relay.ClientIDMutation):
    """
    Mutation for updating an Inventory .
    """
    inventory= graphene.Field(InventoryNode)
    
    class Input:
        id = graphene.ID(required=True, description="Inventory ID.")
        item = graphene.ID(description="Inventory item ID" )
        tag = graphene.String(description="RFID Tag")
        serial_no = graphene.String(description="Inventory Serial No. Max 254 characters")
        seat_no = graphene.String(description="Inventory seat no location")
        location = graphene.ID(description="Inventory location ID" )
        status = graphene.ID(description="Inventory status" )
        
    def mutate_and_get_payload(root, info, **input):
        id = input.get("id")
        try:
            inventory= Inventory.objects.get(pk=from_global_id(id)[1])
            if input.get("item"):
                item = InventoryItem.objects.get(pk=from_global_id(input.get("item"))[1])
                inventory.item = item
            if input.get("location"):
                location = InventoryLocation.objects.get(pk=from_global_id(input.get("location"))[1])
                inventory.location = location
            if input.get("status"):
                status = ItemStatus.objects.get(pk=from_global_id(input.get("status"))[1])
                inventory.status = status
            if input.get("tag"):
                inventory.tag = input.get("tag")
            if input.get("serial_no"):
                inventory.serial_no = input.get("serial_no")
            if input.get("seat_no"):
                inventory.seat_no = input.get("seat_no")
            inventory.save()
        except:
            raise Exception("The Inventory does not exist.")
        
        return UpdateInventory(inventorylocation=inventorylocation)
    
    
class DeleteInventory(graphene.relay.ClientIDMutation):
    """
    Mutation for deleting an Inventory Location.
    """
    success = graphene.Boolean()
    
    class Input:
        id = graphene.ID(required=True, description="Inventory ID")
    
    def mutate_and_get_payload(root, info, **input):
        try:
            inventory= Inventory.objects.get(pk=from_global_id(input.get("id"))[1])
            inventory.delete()
        except:
            Exception("The Inventory you are trying to delete does not exist")
        
        return DeleteInventory(success=True)
            
# -------------------------------------------------------

class Query(graphene.ObjectType):
    inventory_item = graphene.relay.Node.Field(InventoryItemNode, description="Returns an InventoryItem by the given id or an error if InventoryItem does not exist.")
    all_inventory_items = DjangoFilterConnectionField(InventoryItemNode, filterset_class=InventoryItemFilter, description="Returns a list of InventoryItem Types")
    inventory_location = graphene.relay.Node.Field(InventoryLocationNode, description="Returns an InventoryLocation by the given id or an error if InventoryLocation does not exist.")
    all_inventory_locations = DjangoFilterConnectionField(InventoryLocationNode, filterset_class=InventoryLocationFilter, description="Returns a list of InventoryLocation Types")
    inventory = graphene.relay.Node.Field(InventoryNode, description="Returns an Inventory by the given id or an error if Inventory does not exist.")
    all_inventories = DjangoFilterConnectionField(InventoryNode, filterset_class=InventoryFilter, description="Returns a list of Inventory Types")


class Mutation(graphene.ObjectType):
    create_inventory_item = CreateInventoryItem.Field()
    update_inventory_item = UpdateInventoryItem.Field()
    delete_inventory_item = DeleteInventoryItem.Field()
    create_inventory_location = CreateInventoryLocation.Field()
    update_inventory_location = UpdateInventoryLocation.Field()
    delete_inventory_location = DeleteInventoryLocation.Field()
    create_inventory = CreateInventory.Field()
    update_inventory = UpdateInventory.Field()
    delete_inventory = DeleteInventory.Field()