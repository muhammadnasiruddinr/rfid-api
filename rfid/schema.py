import graphene

from inventory.schema import Query as QueryInventory, Mutation as MutationInventory
from master.schema import Query as QueryMaster, Mutation as MutationMaster

class Query(QueryInventory,QueryMaster,graphene.ObjectType):
    pass

class Mutation(MutationInventory,MutationMaster,graphene.ObjectType):
    pass

schema=graphene.Schema(query=Query, mutation=Mutation)